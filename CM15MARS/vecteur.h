#include <string>

class vecteur
{
	double *D;
	int dim;
	string name;

public:
	vecteur(int n, string &nom);
	vecteur(int n, double *T, string &nom);
	~vecteur();
	void affiche();
	double norm();
};