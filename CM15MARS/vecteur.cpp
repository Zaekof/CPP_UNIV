#include "vecteur.h"
#include <iostream>
#include <math>

using namespace std;

vecteur::vecteur(int n, string &nom)
{
	cout << "le constructeur a ete appele ici" << endl;
	dim = n;
	name = nom;
	D = new double[n];

	for(int i = 0; i < n; i++)
	{
		D[i] = 0.0;
	}
}


vecteur::vecteur(int n, double *T, string &nom)
{
	cout << "le constructeur a ete appele ici" << endl;
	dim = n;
	name = nom;
	D = new double[n];

	for(int i = 0; i < n; i++)
	{
		D[i] = 0.0;
	}
}



vecteur::~vecteur()
{
	cout << "test delete" << endl;
	delete [] D;
}

void vecteur::affiche()
{
	cout << "[";
	for (int i = 0; i < dim; i++)
	{
		cout << D[i] << " ";
	}
	cout << "]";
}

double vecteur::nom()
{
	som_c = 0.0;

	for (int i = 0; i < dim; i++)
	{
		som_c += D[i]*D[i];
	}

	return sqrt(som_c);
}

vecteur vecteur::somme(vecteur &v)
{
	string r = "rien";
	vecteur s(dim,D,r);

	for (int i = 0; i < dim; i++)
	{
		s.D[i] += v.D[i];
	}
	return (*s);
}



