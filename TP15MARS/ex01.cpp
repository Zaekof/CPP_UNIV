//NOGIER LOIC
//G3
#include <iostream>
#include "ex01.h"

using namespace std;

collection_entiers::collection_entiers(int n) //constructeur
{
    Nmax = n;
    D = new int[Nmax];
    nbe = Nmax;
    cout << "Construction" << endl;
    cout << nbe << endl;
}

void collection_entiers::afficher()
{
	cout << "Affichage : [" ;
	for(int i = 0; i < nbe; i++)
    {
        D[i] = i;
        cout << D[i] << " ";
    }
	cout << "]" << endl;
}

collection_entiers::~collection_entiers() //destructeur
{
    delete [] D;
    cout << "Destruction" << endl;
    cout << nbe << endl;

	cout << "Affichage : [" ;
    for(int i = 0; i < nbe; i++)
    {
        cout << D[i] << " ";
    }
	cout << "]" << endl;
}